import numpy

import check
import create
from p2m import P2M
from datatypes import particle_dtype

class TestP2M:
    def test_ones(self):
        child = check.deindex(create.cells([0], [0], [0]))
        sources = numpy.ones((10), dtype=particle_dtype)
        sources['m'] = 1

        P2M(child, sources)

        result = check.deindex(create.cells([0], [0], [0]))
        result['multipole'] = numpy.ones(10)*5
        result['multipole'][1:4] = -10
        result['multipole'][0] = 10

        assert result == child

    def test_zeros_cell0(self):
        child = check.deindex(create.cells([0], [0], [0]))
        sources = numpy.zeros((10), dtype=particle_dtype)
        sources['m'] = 1

        P2M(child, sources)

        result = check.deindex(create.cells([0], [0], [0]))
        result['multipole'] = numpy.zeros(10)
        result['multipole'][0] = 10

        assert result == child

    def test_zeros_cell1(self):
        child = check.deindex(create.cells([1], [1], [1]))
        sources = numpy.zeros((10), dtype=particle_dtype)
        sources['m'] = 1

        P2M(child, sources)

        result = check.deindex(create.cells([1], [1], [1]))
        result['multipole'] = numpy.ones(10)*5
        result['multipole'][:4] = 10

        assert result == child
