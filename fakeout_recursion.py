from numba import njit

def add_one(x):
    if x % 2 == 0:
        return x + 2
    else:
        fakeout(x)

def fakeout(x):
    x += 1
    add_one(x)
