import numpy

import calc
import create


class TestEvals:
    def test_distance_ones(self):
        center = create.cells([0], [0], [0])
        cells = create.cells([1, 0, 0], [0, 1, 0], [0, 0, 1])
        distances = calc.distance(cells, center)

        assert numpy.allclose(distances, numpy.ones(3))

    def test_distance_zeros(self):
        center = create.cells([0], [0], [0])
        cells = create.cells([0, 0, 0], [0, 0, 0], [0, 0, 0])
        distances = calc.distance(cells, center)

        assert numpy.allclose(distances, numpy.zeros(3))

    def test_distance_commutativity(self):
        a = create.n_random_particles(10, 1, 1)
        b = create.n_random_particles(10, 1, 1)

        assert numpy.allclose(calc.distance(a,b), calc.distance(b,a))
