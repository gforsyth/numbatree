from numba import njit
import numpy

from datatypes import (particle_dtype,
                       coord_dtype,
                       multipole_dtype,
                       cell_dtype,
                       treecell_dtype)


@njit
def n_random_particles(n, m, domain):
    '''
    Creates `n` particles with mass `m` with random coordinates
    between 0 and `domain`
    '''
    parts = numpy.zeros((n), dtype=particle_dtype)

    for i in parts:
        i.x = numpy.random.random() * domain #attribute access only in @jitted function
        i.y = numpy.random.random() * domain
        i.z = numpy.random.random() * domain
        i.m = m
        i.phi = 0
    return parts

@njit
def cells(x, y, z):
    '''
    Inputs:
    x -- list or array of x-coordinates
    y -- list or array of y-coordinates
    z -- list or array of z-coordinates

    Outputs:
    cells -- array of cells with coords (x, y, z)
             custom datatype
    '''
    n = len(x)
    cells = numpy.zeros(n, dtype=treecell_dtype)
    for i, cell in enumerate(cells):
        cell.x = x[i] #attribute access only in @jitted function
        cell.y = y[i]
        cell.z = z[i]

    return cells

@njit
def children(parent):
    '''
    Generates the coordinates of the 8 child cells in the oct-cube given the
    coordinates of the parent cell.

    Inputs:
    parent -- cell with coords (x,y,z) one step up in the tree
    '''
    octant = numpy.arange(8)
    x = (octant % 2) * parent.x + parent.y/2
    y = ((octant//2) % 2) * parent.y + parent.y/2
    z = ((octant//4) % 2) * parent.z + parent.z/2
    children = cells(x, y, z)

    return children
