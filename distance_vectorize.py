from numba import guvectorize, float64
import numpy

@guvectorize([(float64[:], float64[:], float64[:])], '(n),(n)->()', nopython=True, target='cpu')
def distance_vec(a, b, res):

    #if this is res = ((.... then it's all wonky. good example for tutorial
    res[0] = ((a[0] - b[0])**2 +
              (a[1] - b[1])**2 +
              (a[2] - b[2])**2)**.5

def distance_van(a, b):
    return ((a[0] - b[0])**2 + (a[1] - b[1])**2 + (a[2] - b[2])**2)**.5

#a = numpy.ones(3)
#b = numpy.ones(3)*2
#
#print(distance_vec(a,b))
#print(distance_van(a,b))
#
##a = numpy.ones((3, 10))
##b = numpy.ones(3)*2
##
##print(distance_vec(a,b))
##print(distance_van(a,b))
#
#
#a = numpy.ones((10, 3))
#b = numpy.ones(3)*2
#
#print(distance_vec(a,b))
#print(distance_van(a,b))

#parallel will offer a boost if arrays are ~50000,3 on theo
