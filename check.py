from numba import types, generated_jit

@generated_jit
def deindex(parent):
    if isinstance(parent, types.Array):
        return lambda parent: parent[0]
    elif isinstance(parent, types.Record):
        return lambda parent: parent
    else:
        return lambda parent: False

@generated_jit
def safesum(M):
    if isinstance(M, types.Array):
        return lambda M: M.sum()
    else:
        return lambda M: M
