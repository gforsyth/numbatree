import calc
import create
import evaluate
from tree import (add_child, run_tree_with_L2)

import numpy

def test_add_child_all_octants():
    center = create.cells([0.5],[0.5],[0.5])
    center['r'] = 0.5

    root = [center]

    for i in range(8):
        add_child(i, 0, root)

    assert(len(root) == 9)

def test_tree_theta_zero():
    #if theta is 0 then direct_summation and treecode should return
    #identical answers

    theta = 0.00
    l2norm = run_tree_with_L2(500, theta)

    assert l2norm < 1e-15

def test_tree_theta_half():
    #we expect an l2norm of around 1e-3 for 1000 points with theta=0.5

    theta = 0.5
    l2norm = run_tree_with_L2(1000, theta)

    assert l2norm < 2e-3
