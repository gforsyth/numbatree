from numba import njit
import numpy

import check
import create


@njit
def P2M(child, sources):
    """
    Compute the multipole of a cell based on the strengths and positions of
    the source particles contained within it.

    Parameters
    ----------
    child: array of particle_dtype
        cell containing sources
    multipole: array of multipole_dtype
        multipole array to be calculated
    sources: array of particle_dtype
        source particles to calculate multipole
    """
    child = check.deindex(child)

    dx = child.x - sources.x
    dy = child.y - sources.y
    dz = child.z - sources.z
    M = sources.m

    child.multipole[0] += check.safesum(M)
    child.multipole[1] += check.safesum(M*dx)
    child.multipole[2] += check.safesum(M*dy)
    child.multipole[3] += check.safesum(M*dz)
    child.multipole[4] += check.safesum(M*dx**2/2)
    child.multipole[5] += check.safesum(M*dy**2/2)
    child.multipole[6] += check.safesum(M*dz**2/2)
    child.multipole[7] += check.safesum(M*dx*dy/2)
    child.multipole[8] += check.safesum(M*dy*dz/2)
    child.multipole[9] += check.safesum(M*dz*dx/2)
