from numba import njit
import numpy

import calc
import check


@njit
def potential(targets, center):
#    x, y, z = deindex(center).x, deindex(center).y, deindex(center).z
    x, y, z = center.x, center.y, center.z
    dx, dy, dz = (targets.x - x, targets.y - y, targets.z - z)
    r = calc.distance(targets, center)
    center = check.deindex(center)
    r3 = r**3
    r5 = r**5

    return (center.multipole[0]*1/r +
            center.multipole[1]*-dx/r3 +
            center.multipole[2]*-dy/r3 +
            center.multipole[3]*-dz/r3 +
            center.multipole[4]*(3*dx**2/r5 - 1/r3) +
            center.multipole[5]*(3*dy**2/r5 - 1/r3) +
            center.multipole[6]*(3*dz**2/r5 - 1/r3) +
            center.multipole[7]*3*dx*dy/r5 +
            center.multipole[8]*3*dy*dz/r5 +
            center.multipole[9]*3*dz*dx/r5)


def tree(particles, p, i, cells, theta):

    #ncrit hardcoded as 10
    #for a non leaf cell
    if cells[p]['nleaf'] >= 10:
        for octant in range(8):
            if cells[p]['child'][octant] >= 0:
                c = cells[p]['child'][octant]
                r = calc.distance(particles[i], cells[c])

                #if near-field (theta is always < 1)
                if cells[c]['r'] > theta * r:
                    tree(particles, c, i, cells, theta)
                #if far_field
                else:
                    particles[i]['phi'] += potential(particles[i], cells[c])

    #leaf cell
    else:
        leafnum = cells[p]['nleaf']
        source = particles[cells[p]['leaf'][:leafnum]]
        calc.direct_summation_single(source, particles[i])


def full_potential(particles, cells, theta):
    for i in range(len(particles)):
        tree(particles, 0, i, cells, theta)
