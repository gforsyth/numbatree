import numpy
from numba import njit

import check

@njit
def multipole(parent, child):
    parent = check.deindex(parent)
    dx, dy, dz = (parent.x - child.x,
                    parent.y - child.y,
                    parent.z - child.z)

    for j in range(parent.multipole.shape[0]):
        parent.multipole[j] += child.multipole[j]

    parent.multipole[1] += child.multipole[0] * dx
    parent.multipole[2] += child.multipole[0] * dy
    parent.multipole[3] += child.multipole[0] * dz
    parent.multipole[4] += (child.multipole[1] * dx +
                            0.5 * child.multipole[0] * dx**2)
    parent.multipole[5] += (child.multipole[2] * dy +
                            0.5 * child.multipole[0] * dy**2)
    parent.multipole[6] += (child.multipole[3] * dz +
                            0.5 * child.multipole[0] * dz**2)
    parent.multipole[7] += (0.5 * child.multipole[2] * dx +
                            0.5 * child.multipole[1] * dy +
                            0.5 * child.multipole[0] * dx*dy)
    parent.multipole[8] += (0.5 * child.multipole[3] * dy +
                            0.5 * child.multipole[2] * dz +
                            0.5 * child.multipole[0] * dy*dz)
    parent.multipole[9] += (0.5 * child.multipole[1] * dz +
                            0.5 * child.multipole[3] * dx +
                            0.5 * child.multipole[0] * dz*dx)

#TODO try this using vectorize
@njit
def distance(a, b):
    return ((a.x - b.x)**2 + (a.y - b.y)**2 +
                   (a.z - b.z)**2)**.5

    return dist

#TODO remove return statement (it doesn't do anything)
@njit
def direct_summation(sources, targets):
    for i, target in enumerate(targets):
        for j, source in enumerate(sources):
            if i != j:
                r = distance(target, source)
                target.phi += source.m/r

    return targets

@njit
def direct_summation_single(sources, targets):
    for source in sources:
        r = distance(targets, source)
        if r != 0:
            targets.phi += source.m/r


@njit
def octant(source, parent):
    #source = check.deindex(source)
    parent = check.deindex(parent)

    octant = ((source.x > parent.x) +
              (source.y > parent.y) * 2 +
              (source.z > parent.z) * 4)

    return octant

@njit
def L2_norm(a, b):
    top = 0
    bottom = 0
    for i in range(len(a)):
        top += (a[i] - b[i])**2
        bottom += a[i]**2

    return (top/bottom)**.5
