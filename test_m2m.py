import check
import create
from m2m import M2M
from datatypes import cell_dtype

import numpy

class TestM2M:
    def test_arange(self):
        parent = check.deindex(create.cells([0.5], [0.5], [0.5]))
        children = create.children(parent)

        for i, _ in enumerate(children):
            children[i]['multipole'] = numpy.arange(10)

        M2M(parent, children)

        result = numpy.ones(1, dtype=cell_dtype)
        result['multipole'] = numpy.arange(10)*8

        assert numpy.allclose(result['multipole'], parent['multipole'])

    def test_zeros(self):
        parent = check.deindex(create.cells([0.5], [0.5], [0.5]))
        children = create.children(parent)

        M2M(parent, children)

        result = numpy.zeros(1, dtype=cell_dtype)

        assert numpy.allclose(result['multipole'], parent['multipole'])
