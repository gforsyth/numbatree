from numba import jit, njit
import numpy

import calc
import create
from datatypes import particle_dtype, coord_dtype, multipole_dtype, cell_dtype

@njit
def M2M(parent, children):
    """
    Calculate multipole of parent cell with a weighted average of the child
    multipoles

    Parameters
    ---------
    parent: array of cell_dtype
        parent particle
    children: array of array of cell_dtype
        child particles
    """
    for child in children:
        calc.multipole(parent, child)


