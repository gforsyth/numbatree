import numpy
from numba import njit

import calc
import create
import evaluate
from p2m import P2M
from m2m import M2M


def test_p2m_eval():
    ns = [500, 1000]#, 2000, 3000, 4000, 5000]
    error = []
    for n in ns:

        targets = create.n_random_particles(n, .5, -1)
        sources = create.n_random_particles(n, .5, 1)

        center = create.cells([0.5], [0.5], [0.5])

        P2M(center, sources)
        phi = evaluate.potential(targets, center)

        phi_direct = calc.direct_summation(sources, targets)

        err = numpy.sqrt(sum((phi_direct['phi']-phi)**2)/sum(phi_direct['phi']**2))

        error.append(err)

    print(error)
    assert (numpy.array(error) < .005).all()

def test_m2m_eval():
    error = m2m_eval()
    print(error)
    assert (numpy.array(error) < .005).all()

@njit
def m2m_eval():
    ns = [500, 1000]#, 2000, 3000, 4000, 5000]
    error = []
    for n in ns:

        targets = create.n_random_particles(n, 1, -1)
        sources = create.n_random_particles(n, 1, 1)

        center = create.cells([0.5], [0.5], [0.5])
        #create octants from parent cell
        children = create.children(center)

        for source in sources:
            #calculate octant that given source belongs to
            octant = calc.octant(source, center)
            #calculate contribution to appropriate child multipole
            P2M(children[octant], source)

        #calculate parent multipole as weighted average of child multipoles
        M2M(center, children)

        # calculate phi based on aggregate multipole
        phi = evaluate.potential(targets, center)

        phi_direct = calc.direct_summation(sources, targets)

        err = calc.L2_norm(phi_direct['phi'], phi)

        error.append(err)

    return error
