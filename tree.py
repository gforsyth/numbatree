from numba import njit
import numpy

import calc
import create
import check
import evaluate
from p2m import P2M
from m2m import M2M


def deindex(a):
    if isinstance(a, numpy.ndarray):
        return a[0]
    else:
        return a


def add_child(octant, parent, cell_list):
    #note that n_crit is hardcoded as 10
    #cell_list is an array of size num_particles/10 (which is overkill but necessary because a list of custom dtypes chokes numba)
    #unless we do this as a pure python function
    cell = deindex(cell_list[parent])
    r = cell['r'] / 2
    x = cell['x'] + r * ((octant & 1) * 2 - 1)
    y = cell['y'] + r * ((octant & 2) - 1)
    z = cell['z'] + r * ((octant & 4) / 2 - 1)

    newcell = create.cells([x], [y], [z])
    newcell['child'] = numpy.ones(8) * -1
    newcell['r'] = r
    newcell['parent'] = parent
    cell_list.append(newcell)
    cell['child'][octant] = len(cell_list) - 1

def split_cell(particles, parent_index, cell_list):
    #n_crit hardcoded as 10
    p = parent_index
    #print('Splitting cell {}'.format(p))
    cell_list[p] = deindex(cell_list[p])
    for l in cell_list[p]['leaf']:
        #contents of 'leaf' are indices of corresponding particles in particle list
        octant = calc.octant(particles[l], cell_list[p])
        if cell_list[p]['child'][octant] < 0:
            add_child(octant, p, cell_list)

        c = reallocate(cell_list, octant, p, l)

        if cell_list[c]['nleaf'] >= 10:  #ncrit
            split_cell(particles, c, cell_list)

def reallocate(cell_list, octant, p, l):

    #reallocate particles into child cell
    cell_list[p] = deindex(cell_list[p])
    c = cell_list[p]['child'][octant]
    cell_list[c] = deindex(cell_list[c])
    cell_list[c]['leaf'][cell_list[c]['nleaf']] = l

    cell_list[c]['nleaf'] += 1
    #print('particle {} is reallocated in cell {}'.format(l, c))

    return c

def build_tree(particles, root):

    cells = [root]

    n = len(particles)

    for i in range(n):
        curr = 0
        cells[curr] = deindex(cells[curr])
        while cells[curr]['nleaf'] >= 10:  #ncrit
            cells[curr]['nleaf'] += 1
            octant = calc.octant(particles[i], cells[curr])
            #if no child cell in particles octant, create
            if cells[curr]['child'][octant] < 0:
                add_child(octant, curr, cells)

            curr = cells[curr]['child'][octant]

        #allocate particles in leaf cells
        cells[curr] = deindex(cells[curr])
        cells[curr]['leaf'][cells[curr]['nleaf']] = i
        cells[curr]['nleaf'] += 1
        #print('particle {} is stored in cell {}'.format(i, curr))

        if cells[curr]['nleaf'] >= 10:
            split_cell(particles, curr, cells)

    return cells

def calculate_all_multipoles(particles, p, cells):
    '''
    Recursively travel down the tree until you reach a leaf cell (one in which
    there are 10 or fewer particles).  Then sweeep up the tree, calculating
    the multipoles from either particles or existing child multipoles

    ncrit hardcoded equal to 10
    '''
    cells[p] = deindex(cells[p])
    if cells[p]['nleaf'] >= 10: #nleaf count does not reset on split/reallocate
        for c in range(8):
            if cells[p]['child'][c] >= 0: #e.g. if not still -1 and so nonexistant
                calculate_all_multipoles(particles, cells[p]['child'][c], cells)
    else:
        for i in range(cells[p]['nleaf']):
            l = cells[p]['leaf'][i] #get particle index
            P2M(cells[p], particles[l])

def upward_sweep(cells):
    '''
    Traverse from leaves to root of tree calculating M2M along the way
    '''
    for c in range(len(cells) - 1, 0, -1):
        p = cells[c]['parent']
        calc.multipole(cells[p], cells[c])


def run_tree_with_L2(n, theta):
    center = create.cells([0.5], [0.5], [0.5])
    center['r'] = 0.5
    center['child'] = numpy.ones(8) * -1

    particles = create.n_random_particles(n, 1, 1)

    calc.direct_summation(particles, particles)

    phi_direct = numpy.asarray([particle['phi'] for particle in particles])
    for particle in particles:
        particle['phi'] = 0

    cells = build_tree(particles, center)

    calculate_all_multipoles(particles, 0, cells)

    upward_sweep(cells)

    evaluate.full_potential(particles, cells, theta)

    phi_tree = numpy.asarray([particle['phi'] for particle in particles])

    return calc.L2_norm(phi_direct, phi_tree)

def run_tree(n, theta):
    center = create.cells([0.5], [0.5], [0.5])
    center['r'] = 0.5
    center['child'] = numpy.ones(8) * -1

    particles = create.n_random_particles(n, 1, 1)

    cells = build_tree(particles, center)

    calculate_all_multipoles(particles, 0, cells)

    upward_sweep(cells)

    evaluate.full_potential(particles, cells, theta)

    return cells, particles
