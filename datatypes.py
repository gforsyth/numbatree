import numpy

cell_dtype = numpy.dtype({'names':['x','y','z','multipole'],
                    'formats':[numpy.float64,
                               numpy.float64,
                               numpy.float64,
                               (numpy.float64, 10)]})

#TODO: rearrange this, it's hard to read
treecell_dtype = numpy.dtype({'names':['x','y','z','r', 'multipole',
                                       'nleaf', 'leaf', 'nchild', 'child',
                                       'parent'],
                    'formats':[numpy.float64,
                               numpy.float64,
                               numpy.float64,
                               numpy.float64,
                               (numpy.float64, 10), #multipole
                               numpy.int32,       #nleaf
                               (numpy.int32, 10), #leaf
                               numpy.int32,       #nchild
                               (numpy.int32, 8),  #child
                               numpy.int32,]})    #parent

particle_dtype = numpy.dtype({'names':['x','y','z','m','phi'],
                         'formats':[numpy.double,
                                    numpy.double,
                                    numpy.double,
                                    numpy.double,
                                    numpy.double]})

coord_dtype = numpy.dtype({'names':['x','y','z'],
                    'formats':[numpy.float64,
                               numpy.float64,
                               numpy.float64,]})

multipole_dtype = numpy.dtype({'names':['multipole'],
                    'formats':[(numpy.float64, 10)]})
