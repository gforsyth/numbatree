from calc import L2_norm
import numpy

def test_l2_norm():
    a = numpy.random.random(50)
    b = numpy.random.random(50)

    err = numpy.sqrt(sum((a-b)**2)/sum(a**2))
    assert L2_norm(a,b) == err
